import UIKit

enum TipoComputador {
    case
    
    gamer,
    oficina
}

struct Procesador {
    var id: String
    var marca: String
    var modelo: String
}
struct RAM {
    var id: String
    var marca: String
    var cantidadRAM: String
}
struct GPU {
    var id: String
    var marca: String
    var cantidadVRAM: String
}

protocol PCNormalBuilder{
    var tipo: TipoComputador { get set }
    var procesador: Procesador { get set }
    var gpu: GPU {get set}
    var ram: RAM {get set}
    var enfriacionLiquida: Bool { get set }
}

class PC: CustomStringConvertible,PCNormalBuilder {
    var tipo: TipoComputador
    var ram: RAM
    var gpu: GPU
    var procesador: Procesador
    var enfriacionLiquida: Bool
    
    var description: String {
        return "Tipo: \(tipo)\nProcesador: \(procesador)\nRAM: \(ram)\nTarjeta de Video: \(gpu)\nEnfriacion Liquida: \(enfriacionLiquida)"
    }
    
    init(tipo: TipoComputador, procesador: Procesador, ram: RAM, gpu:GPU , enfriacionLiquida: Bool) {
        self.tipo = tipo
        self.procesador = procesador
        self.gpu = gpu
        self.ram = ram
        self.enfriacionLiquida = enfriacionLiquida
        
    }
}

class PCBuilder:  PCNormalBuilder {
    var tipo: TipoComputador = .oficina
    var procesador: Procesador = Procesador(id: "15-101", marca: "Intel",
                             modelo: "Core i5-3210")
    
    var gpu: GPU = GPU(id: "14-123", marca: "Intel HD Graphics",
                       cantidadVRAM: "2 GB")
    var ram: RAM = RAM(id: "13-444", marca: "Kingston",
                       cantidadRAM: "4 GB")
    
    var enfriacionLiquida: Bool = false
    
    func buildPC() -> PC {
        return PC(tipo: tipo, procesador : procesador,ram:ram, gpu : gpu, enfriacionLiquida :  enfriacionLiquida)
    }
}
print("\nPC por Defecto:")
var builder = PCBuilder()
let pcporDefecto = builder.buildPC()
print(pcporDefecto)
print("\n")

print("\nPC Custom:")
builder.procesador.marca = "AMD"
builder.tipo = .gamer
builder.ram.marca = "HyperX"
builder.ram.cantidadRAM = "16 GB"
builder.enfriacionLiquida = true
let pcCustom = builder.buildPC()
print(pcCustom)
